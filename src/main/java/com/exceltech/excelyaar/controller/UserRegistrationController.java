package com.exceltech.excelyaar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.exceltech.excelyaar.data.dto.UserRegistrationDto;
import com.exceltech.excelyaar.service.security.UserService;


@Controller
@RequestMapping
public class UserRegistrationController {

	private UserService userService;

	public UserRegistrationController(UserService userService) {
		super();
		this.userService = userService;
	}
	
	@ModelAttribute("user")
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }
	
	@GetMapping("/registration")
	public ModelAndView showRegistrationForm() {
		return new ModelAndView("web/registration");
	}
	@GetMapping("/adminregistration")
	public ModelAndView showAdminRegistrationForm() {
		return new ModelAndView("web/registration-admin");
	}

	@PostMapping("/registration")
	public ModelAndView registerUserAccount(@ModelAttribute("user") UserRegistrationDto registrationDto) {
		userService.save(registrationDto);
		
		 ModelAndView mv = new ModelAndView("web/registration");
		 mv.addObject("success","success");
		return mv;
	}
	@PostMapping("/adminregistration")
	public ModelAndView registerAdminUserAccount(@ModelAttribute("user") UserRegistrationDto registrationDto) {
		userService.save(registrationDto);
		
		 ModelAndView mv = new ModelAndView("web/registration");
		 mv.addObject("success","success");
		return mv;
	}

}