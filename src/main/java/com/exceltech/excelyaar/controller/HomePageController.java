package com.exceltech.excelyaar.controller;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

@SessionAttributes(names = {"user"})
@RestController
public class HomePageController {
	
	@GetMapping("/connecter")
	public ModelAndView connecter() {
		return new ModelAndView("web/login");
	}
	
	@RequestMapping("/login")
	public ModelAndView login() {
		System.out.println("/login called");
		return new ModelAndView("web/login");
	}
	@GetMapping("/logout")
	protected ModelAndView loginOut(HttpServletRequest request, HttpServletResponse response){
	        
		      HttpSession session = request.getSession(false);
		      SecurityContextHolder.clearContext();

		      session = request.getSession(false);
		      if(session != null) {
		          session.invalidate();
		      }

		      for(Cookie cookie : request.getCookies()) {
		          cookie.setMaxAge(0);
		      }

	    return new ModelAndView("web/home");
	}
	
	 @RequestMapping("/home")
	 @GetMapping
	    public ModelAndView welcome(){
	        return new ModelAndView("web/index");
	    }
	 @RequestMapping("/")
	 @GetMapping
	    public ModelAndView index(){
	        return new ModelAndView("web/index");
	    }
	 @RequestMapping("/store-setting")
	 @GetMapping
	    public ModelAndView storeSetting(){
	        return new ModelAndView("web/storeSetting");
	    }
	    
	     @RequestMapping("/shop-default")
	 @GetMapping
	    public ModelAndView shopDefault(){
	        return new ModelAndView("web/shop-default");

	     }
	     @RequestMapping("/devenir-vendeur")
	 @GetMapping
	    public ModelAndView becomeVendor(){
	        return new ModelAndView("web/become-a-vendor");
	    }

}
