package com.exceltech.excelyaar.controller;

import java.security.Principal;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.exceltech.excelyaar.data.dto.BecomeVendorDto;
import com.exceltech.excelyaar.data.dto.UserRegistrationDto;
import com.exceltech.excelyaar.service.security.UserService;
import com.exceltech.excelyaar.service.security.model.RequestUser;
import com.exceltech.excelyaar.service.vendors.BecomeVendorService;


@Controller
@RequestMapping("/store-setting")
public class BecomeVendorController {

	private BecomeVendorService becomeVendorService;

	public BecomeVendorController(BecomeVendorService becomeVendorService) {
		super();
		this.becomeVendorService = becomeVendorService;
	}
	
	@ModelAttribute("vendor")
    public BecomeVendorDto becomeVendorDto() {
        return new BecomeVendorDto();
    }
	
	
	@GetMapping
	public ModelAndView showVendorRegistrationForm() {
		return new ModelAndView("web/storeSetting");
	}
	
	@PostMapping
	public ModelAndView registerVendorAccount(
			@AuthenticationPrincipal  Principal principal,
			@ModelAttribute("vendor") BecomeVendorDto becomeVendorDto,
			BindingResult bindingResult) {
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	      String name = user.getUsername(); 
		becomeVendorService.save(becomeVendorDto);
		 ModelAndView mv = new ModelAndView("web/storeSetting"); 
		 mv.addObject("success","success");
		return mv;
	}
}