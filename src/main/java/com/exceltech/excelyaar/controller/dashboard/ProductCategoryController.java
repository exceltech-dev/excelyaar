package com.exceltech.excelyaar.controller.dashboard;


import com.exceltech.excelyaar.controller.dashboard.resource.ProductCategoryForm;
import com.exceltech.excelyaar.data.dto.BecomeVendorDto;
import com.exceltech.excelyaar.service.categories.ProductCategoriesService;
import com.exceltech.excelyaar.service.categories.model.ProductCategoryDto;
import com.exceltech.excelyaar.service.categories.model.ProductSubCategoryDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
public class ProductCategoryController {

    private ProductCategoriesService productCategoriesService;

    public ProductCategoryController(ProductCategoriesService productCategoriesService) {
		super();

        this.productCategoriesService = productCategoriesService;
    }

    @GetMapping("/show-categories")
    public ModelAndView showProductCategory(@RequestParam("page") Optional<Integer> page,
                                            @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
        ModelAndView model = getCategory(currentPage, pageSize);

        return model;

    }

    @GetMapping("/subcategories")
    public ModelAndView showProductSubCategory(@RequestParam("page") Optional<Integer> page,
                                               @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        ModelAndView model = getSubCategory(currentPage, pageSize);
        return model;

    }

    @GetMapping("/subcategories/{Ref}")
    public ModelAndView showSpecificProductSubCategory(@RequestParam("page") Optional<Integer> page,
                                                       @RequestParam("size") Optional<Integer> size, @PathVariable("Ref") final UUID ref) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);


        ModelAndView model = new ModelAndView("admin/subcategories");

        Page<ProductSubCategoryDto> productSubCategoryDtoPage = productCategoriesService.getSpecificAllProductSubCategory(ref, PageRequest.of(currentPage - 1, pageSize));
        model.addObject("subcategoriesPage", productSubCategoryDtoPage);

        int totalPages = productSubCategoryDtoPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addObject("pageNumbers", pageNumbers);
        }

        return model;

    }
    
	@ModelAttribute("categories")
    public ProductCategoryForm productCategoryForm() {
        return new ProductCategoryForm();
    }
	
    @PostMapping("/show-categories")
    public ModelAndView postCategory(
            @AuthenticationPrincipal Principal principal,
            @ModelAttribute("categories") ProductCategoryForm productCategoryForm,
            BindingResult bindingResult) {

        productCategoriesService.storeProductCategory(productCategoryForm);
        int currentPage = 1;
        int pageSize = 5;
        ModelAndView mv = getCategory(currentPage, pageSize);
        return mv;
    }

    @PostMapping("/subcategory")
    public ModelAndView postSubCategory(
            @AuthenticationPrincipal Principal principal,
            @ModelAttribute("categories") ProductCategoryForm productCategoryForm,
            BindingResult bindingResult) {

        productCategoriesService.storeProductCategory(productCategoryForm);
        int currentPage = 1;
        int pageSize = 5;
        ModelAndView mv = getSubCategory(currentPage, pageSize);
        return mv;
    }

    private ModelAndView getSubCategory(int currentPage,
                                        int pageSize) {

        ModelAndView model = new ModelAndView("admin/subcategories");

        Page<ProductSubCategoryDto> productSubCategoryDtoPage = productCategoriesService.getAllSubProductCategory(PageRequest.of(currentPage - 1, pageSize));
        model.addObject("subcategoriesPage", productSubCategoryDtoPage);

        int totalPages = productSubCategoryDtoPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addObject("pageNumbers", pageNumbers);
        }

        return model;
    }

    private ModelAndView getCategory(int currentPage,
                                     int pageSize) {

        ModelAndView model = new ModelAndView("admin/categories");

        Page<ProductCategoryDto> productCategoryDtoPage = productCategoriesService.getAllProductCategory(PageRequest.of(currentPage - 1, pageSize));
        model.addObject("categoriesPage", productCategoryDtoPage);

        int totalPages = productCategoryDtoPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addObject("pageNumbers", pageNumbers);
        }

        return model;
    }
}
