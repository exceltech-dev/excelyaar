package com.exceltech.excelyaar.controller.dashboard.resource;

import com.sun.istack.NotNull;

import java.util.UUID;

public class ProductCategoryForm {

	@NotNull
	private String productType;

	private UUID productTypeId;

	@NotNull
	private String productTypeSlug;

	@NotNull
	private String description;

	public ProductCategoryForm() {

	}

	public ProductCategoryForm(String productType, String productTypeSlug, String description, UUID productTypeId) {
		super();
		this.productType = productType;
		this.productTypeSlug = productTypeSlug;
		this.description = description;
		this.productTypeId = productTypeId;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getProductTypeSlug() {
		return productTypeSlug;
	}

	public void setProductTypeSlug(String productTypeSlug) {
		this.productTypeSlug = productTypeSlug;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public UUID getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(UUID productTypeId) {
		this.productTypeId = productTypeId;
	}

	@Override
	public String toString() {
		return "ProductCategoryForm [productType=" + productType + ", productTypeId=" + productTypeId
				+ ", productTypeSlug=" + productTypeSlug + ", description=" + description + "]";
	}

}
