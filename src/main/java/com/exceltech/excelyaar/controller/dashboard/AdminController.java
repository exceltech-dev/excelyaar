package com.exceltech.excelyaar.controller.dashboard;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.exceltech.excelyaar.data.dto.DescriptionDto;
import com.exceltech.excelyaar.data.entity.VendorEntity;
import com.exceltech.excelyaar.service.vendors.BecomeVendorService;

@RestController
public class AdminController {
	private BecomeVendorService becomeVendorService;

	public AdminController(BecomeVendorService becomeVendorService) {
		super();
		this.becomeVendorService = becomeVendorService;
	}

	@GetMapping("/dashboard")
	public ModelAndView showDashboard() {
		ModelAndView mv = new ModelAndView("admin/index");
//mv.addObject("success","success");
		return mv;
	}
	/*
	 * @GetMapping(value="/boutiques") public ModelAndView showStores() {
	 * ModelAndView mv = new ModelAndView("admin/boutiques"); List<VendorEntity>
	 * vendorEntities=becomeVendorService.getVendors();
	 * System.out.print(vendorEntities); mv.addObject("vendors",vendorEntities);
	 * return mv; }
	 */

	@ModelAttribute("descriptionDto")
	public DescriptionDto descriptionDto() {
		return new DescriptionDto();
	}

	@PostMapping(value = "/voir-description")
	public ModelAndView showVendor(@AuthenticationPrincipal Principal principal,
			@ModelAttribute("descriptionDto") DescriptionDto descriptionDto, BindingResult bindingResult) {
		System.out.print(descriptionDto);

		VendorEntity vendorEntity = becomeVendorService.getVendor(descriptionDto.getVendorId());

		ModelAndView mv = new ModelAndView("admin/vendor-description");
		mv.addObject("vendor", vendorEntity);
		return mv;
	}

	@RequestMapping(value = "/boutiques", method = RequestMethod.GET)
	public ModelAndView listBooks(@RequestParam("page") Optional<Integer> page,
			@RequestParam("size") Optional<Integer> size) {
		int currentPage = page.orElse(1);
		int pageSize = size.orElse(5);
		ModelAndView model = new ModelAndView("admin/boutiques");

		Page<VendorEntity> vendorsPage = becomeVendorService.findPaginated(PageRequest.of(currentPage - 1, pageSize));
	
		model.addObject("vendorsPage", vendorsPage);

		int totalPages = vendorsPage.getTotalPages();
		if (totalPages > 0) {
			List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
			model.addObject("pageNumbers", pageNumbers);
		}

		return model;
	}

	@PostMapping(value = "/vendor-to-activate")
	public ModelAndView activateVendor(@AuthenticationPrincipal Principal principal,
			@ModelAttribute("descriptionDto") DescriptionDto descriptionDto, BindingResult bindingResult) {

		VendorEntity vendorEntity = becomeVendorService.activateVendor(descriptionDto.getVendorId());
		ModelAndView mv = new ModelAndView("admin/vendor-description");
		mv.addObject("vendor", vendorEntity);
		return mv;
	}
}
