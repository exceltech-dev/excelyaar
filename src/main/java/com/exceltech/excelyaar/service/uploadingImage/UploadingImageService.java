package com.exceltech.excelyaar.service.uploadingImage;

import org.springframework.web.multipart.MultipartFile;

public interface UploadingImageService {
	void saveFile(String uploadDir, String fileName,
            MultipartFile multipartFile);
}
