package com.exceltech.excelyaar.service.uploadingImage;


import java.io.*;
import java.nio.file.*;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class UploadingImageServiceImpl implements UploadingImageService {
	
	@Override
	 public  void saveFile(String uploadDir, String fileName,
	            MultipartFile multipartFile) {
	        Path uploadPath = Paths.get(uploadDir);
	        try {
	        if (!Files.exists(uploadPath)) {
	            
					Files.createDirectories(uploadPath);
				
	        }
	         
	       
	        	InputStream inputStream = multipartFile.getInputStream();
	            Path filePath = uploadPath.resolve(fileName);
	            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
	        }
	        catch (IOException ioe) {        
	            throw new RuntimeException("Could not save image file: " + fileName, ioe);
	        }  
   
	    }
}
