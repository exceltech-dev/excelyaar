package com.exceltech.excelyaar.service.vendors;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.exceltech.excelyaar.data.dto.BecomeVendorDto;
import com.exceltech.excelyaar.data.entity.*;

public  interface BecomeVendorService {

	VendorEntity save(BecomeVendorDto becomeVendorDto);
	List<VendorEntity> getVendors();
	 void takePicture();
	 VendorEntity getVendor(Long vendor_id);
	 Page<VendorEntity> findPaginated(Pageable pageable);
	 VendorEntity activateVendor(Long vendor_id);
	 VendorEntity getVendorStoreInformation();
}
