package com.exceltech.excelyaar.service.vendors;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.github.sarxos.webcam.Webcam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.imageio.ImageIO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.exceltech.excelyaar.common.UserRole;
import com.exceltech.excelyaar.data.dto.BecomeVendorDto;
import com.exceltech.excelyaar.data.repository.BecomeVendorRepository;
import com.exceltech.excelyaar.data.repository.UserRepository;
import com.exceltech.excelyaar.service.uploadingImage.UploadingImageService;
import com.exceltech.excelyaar.data.entity.*;

@Service
public class BecomeVendorServiceImpl implements BecomeVendorService {

	private BecomeVendorRepository becomeVendorRepository;
	private UserRepository userRespository;
	private UploadingImageService uploadingImageService;


	public BecomeVendorServiceImpl(BecomeVendorRepository becomeVendorRepository, UserRepository userRespository,
			UploadingImageService uploadingImageService) {
		super();
		this.becomeVendorRepository = becomeVendorRepository;
		this.userRespository = userRespository;
		this.uploadingImageService = uploadingImageService;


	}

	@Override
	public VendorEntity save(BecomeVendorDto becomeVendorDto) {
		// TODO Auto-generated method stub
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String userName = user.getUsername();
		Optional<CustomerEntity> userEntity = userRespository.findByEmail(userName);

		if (userEntity.isPresent()) {
			userEntity.get().setUserRole(UserRole.VENDOR);
			userRespository.save(userEntity.get());
			
			String fileName = StringUtils.cleanPath(becomeVendorDto.getSelfPhotoImage().getOriginalFilename());

			VendorEntity vendor = new VendorEntity();
			vendor.setCustomerEntity(userEntity.get());
			vendor.setFirstName(becomeVendorDto.getFirstName());
			vendor.setLastName(becomeVendorDto.getLastName());
			vendor.setApproved(becomeVendorDto.getApproved());
			vendor.setCreatedDate(new Date());
			vendor.setDescription(becomeVendorDto.getDescription());
			vendor.setEmail(becomeVendorDto.getEmail());
			vendor.setUpdateDate(new Date());
			vendor.setStoreName(becomeVendorDto.getStoreName());

			VendorEntity vendorEntity = becomeVendorRepository.save(vendor);

			vendorEntity = saveVendorPersonalImages(vendorEntity, becomeVendorDto);
			
		}
		return null;
	}


	private VendorEntity saveVendorPersonalImages(VendorEntity vendorEntity, BecomeVendorDto becomeVendorDto) {

		String uploadDir_selfie = "Vendeurs/" + String.valueOf(vendorEntity.getId()) + "/selfPhoto/";
		String uploadDir_idCard = "Vendeurs/" + String.valueOf(vendorEntity.getId()) + "/idCard/";

		String selfPhotoImagefileName = String.valueOf(vendorEntity.getId()) + ".png";
		String idPhotoImagefileName = String.valueOf(vendorEntity.getId()) + ".png";
		uploadingImageService.saveFile(uploadDir_selfie, selfPhotoImagefileName, becomeVendorDto.getSelfPhotoImage());
		uploadingImageService.saveFile(uploadDir_idCard, idPhotoImagefileName, becomeVendorDto.getIdPhotoImage());
		vendorEntity.setSelfPhotoImage(uploadDir_selfie + selfPhotoImagefileName);
		vendorEntity.setIdPhotoImage(uploadDir_idCard + idPhotoImagefileName);
		becomeVendorRepository.save(vendorEntity);
		return vendorEntity;

	}

	@Override
	public List<VendorEntity> getVendors() {
		List<VendorEntity> vendorEntities = becomeVendorRepository.findAll();
		// TODO Auto-generated method stub
		return vendorEntities;
	}

	@Override
	public void takePicture() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = user.getUsername();
		Optional<CustomerEntity> userEntity = userRespository.findByEmail(name);
		// get default webcam and open it
		Webcam webcam = Webcam.getDefault();
		webcam.open();

		// get image
		BufferedImage image = webcam.getImage();

		// save image to PNG file
		try {
			ImageIO.write(image, "PNG", new File(String.valueOf(userEntity.get().getId()) + ".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}
	}

	@Override
	public VendorEntity getVendor(Long vendor_id) {
		// TODO Auto-generated method stub
		Optional<VendorEntity> vendorEntity = becomeVendorRepository.findById(vendor_id);
		return vendorEntity.get();
	}

	@Override
	public VendorEntity activateVendor(Long vendor_id) {
		// TODO Auto-generated method stub
		Optional<VendorEntity> vendorEntity = becomeVendorRepository.findById(vendor_id);
		vendorEntity.get().setApproved(true);
		becomeVendorRepository.save(vendorEntity.get());
		return vendorEntity.get();
	}

	@Override
	public Page<VendorEntity> findPaginated(Pageable pageable) {
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		int startItem = currentPage * pageSize;
		List<VendorEntity> vendors = becomeVendorRepository.findAll();
		List<VendorEntity> list;

		if (vendors.size() < startItem) {
			list = Collections.emptyList();
		} else {
			int toIndex = Math.min(startItem + pageSize, vendors.size());
			list = vendors.subList(startItem, toIndex);
		}

		Page<VendorEntity> bookPage = new PageImpl<VendorEntity>(list, PageRequest.of(currentPage, pageSize),
				vendors.size());

		return bookPage;
	}

	@Override
	public VendorEntity getVendorStoreInformation() {
		// TODO Auto-generated method stub
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = user.getUsername();
		Optional<CustomerEntity> userEntity = userRespository.findByEmail(name);
		Optional<VendorEntity> vendorEntity = becomeVendorRepository.findById(userEntity.get().getId());
		return vendorEntity.get();
	}

}
