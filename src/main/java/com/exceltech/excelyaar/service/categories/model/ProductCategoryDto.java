package com.exceltech.excelyaar.service.categories.model;

import java.util.Date;
import java.util.UUID;

public class ProductCategoryDto {
    private UUID productCategoryId;


    private String productType;

    private String productTypeSlug;

    private String description;


    private String createdBy;


    private String updatedBy;


    private Boolean isActive;

    private Date createdDate;

    private Date lastUpdatedDate;


    private ProductCategoryDto(Builder builder) {

        productCategoryId = builder.productCategoryId;
        productType = builder.productType;

        productTypeSlug = builder.productTypeSlug;

        description = builder.description;

        createdBy = builder.createdBy;

        updatedBy = builder.updatedBy;

        isActive = builder.isActive;

        lastUpdatedDate = builder.lastUpdatedDate;
        createdDate = builder.createdDate;

    }

    public UUID getProductCategoryId() {
        return productCategoryId;
    }

    public String getProductType() {
        return productType;
    }

    public String getProductTypeSlug() {
        return productTypeSlug;
    }

    public String getDescription() {
        return description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public Boolean getActive() {
        return isActive;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    @Override
    public String toString() {
        return "ProductCategoryDto{" +
                "productCategoryId=" + productCategoryId +
                ", productType='" + productType + '\'' +
                ", productTypeSlug='" + productTypeSlug + '\'' +
                ", description='" + description + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", isActive=" + isActive +
                ", createdDate=" + createdDate +
                ", lastUpdatedDate=" + lastUpdatedDate +
                '}';
    }

    public static final class Builder {
        private UUID productCategoryId;


        private String productType;

        private String productTypeSlug;

        private String description;


        private String createdBy;


        private String updatedBy;


        private Boolean isActive;


        private Date lastUpdatedDate;

        private Date createdDate;

        public static Builder builder() {
            return new Builder();
        }

        public Builder productCategoryId(UUID productCategoryId) {
            this.productCategoryId = productCategoryId;
            return this;
        }


        public Builder productTypeSlug(String productTypeSlug) {
            this.productTypeSlug = productTypeSlug;
            return this;
        }

        public Builder productType(String productType) {
            this.productType = productType;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder createdBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public Builder updatedBy(String updatedBy) {
            this.updatedBy = updatedBy;
            return this;
        }

        public Builder isActive(Boolean isActive) {
            this.isActive = isActive;
            return this;
        }

        public Builder lastUpdatedDate(Date lastUpdatedDate) {
            this.lastUpdatedDate = lastUpdatedDate;
            return this;
        }

        public Builder createdDate(Date createdDate) {
            this.createdDate = createdDate;
            return this;
        }


        public ProductCategoryDto build() {
            return new ProductCategoryDto(this);
        }

    }
}
