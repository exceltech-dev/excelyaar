package com.exceltech.excelyaar.service.categories.model;

import com.exceltech.excelyaar.data.entity.ProductCategoryEntity;

import java.util.Date;
import java.util.UUID;

public class ProductSubCategoryDto {
    private UUID productSubCategoryId;

    private ProductCategoryEntity productCategoryId;
    private String productSubType;

    private String productSubTypeSlug;

    private String description;


    private String createdBy;


    private String updatedBy;


    private Boolean isActive;

    private Date createdDate;

    private Date lastUpdatedDate;

    private ProductSubCategoryDto(Builder builder) {

        productSubCategoryId = builder.productSubCategoryId;
        productSubType = builder.productSubType;
        productCategoryId = builder.productCategoryEntity;
        productSubTypeSlug = builder.productSubTypeSlug;

        description = builder.description;

        createdBy = builder.createdBy;

        updatedBy = builder.updatedBy;

        isActive = builder.isActive;

        lastUpdatedDate = builder.lastUpdatedDate;
        createdDate = builder.createdDate;

    }

    public UUID getProductSubCategoryId() {
        return productSubCategoryId;
    }

    public String getProductSubType() {
        return productSubType;
    }

    public String getProductSubTypeSlug() {
        return productSubTypeSlug;
    }

    public String getDescription() {
        return description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public Boolean getActive() {
        return isActive;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public ProductCategoryEntity getProductCategoryId() {
        return productCategoryId;
    }

    @Override
    public String toString() {
        return "ProductSubCategoryDto{" +
                "productSubCategoryId=" + productSubCategoryId +
                ", productCategoryEntity=" + productCategoryId +
                ", productSubType='" + productSubType + '\'' +
                ", productTypeSlug='" + productSubTypeSlug + '\'' +
                ", description='" + description + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", isActive=" + isActive +
                ", createdDate=" + createdDate +
                ", lastUpdatedDate=" + lastUpdatedDate +
                '}';
    }

    public static final class Builder {
        private UUID productSubCategoryId;

        private ProductCategoryEntity productCategoryEntity;
        private String productSubType;

        private String productSubTypeSlug;

        private String description;


        private String createdBy;


        private String updatedBy;


        private Boolean isActive;


        private Date lastUpdatedDate;

        private Date createdDate;

        public static Builder builder() {
            return new Builder();
        }

        public Builder productSubCategoryId(UUID productSubCategoryId) {
            this.productSubCategoryId = productSubCategoryId;
            return this;
        }
        public Builder productCategoryEntity(ProductCategoryEntity productCategoryEntity) {
            this.productCategoryEntity = productCategoryEntity;
            return this;
        }

        public Builder productSubTypeSlug(String productSubTypeSlug) {
            this.productSubTypeSlug = productSubTypeSlug;
            return this;
        }

        public Builder productSubType(String productSubType) {
            this.productSubType = productSubType;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder createdBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public Builder updatedBy(String updatedBy) {
            this.updatedBy = updatedBy;
            return this;
        }

        public Builder isActive(Boolean isActive) {
            this.isActive = isActive;
            return this;
        }

        public Builder lastUpdatedDate(Date lastUpdatedDate) {
            this.lastUpdatedDate = lastUpdatedDate;
            return this;
        }

        public Builder createdDate(Date createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public ProductSubCategoryDto build() {
            return new ProductSubCategoryDto(this);
        }

    }
}
