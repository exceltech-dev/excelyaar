package com.exceltech.excelyaar.service.categories;

import com.exceltech.excelyaar.controller.dashboard.resource.ProductCategoryForm;
import com.exceltech.excelyaar.data.entity.CustomerEntity;
import com.exceltech.excelyaar.data.entity.ProductCategoryEntity;
import com.exceltech.excelyaar.data.entity.ProductSubCategoryEntity;
import com.exceltech.excelyaar.data.repository.*;
import com.exceltech.excelyaar.service.categories.model.ProductCategoryDto;
import com.exceltech.excelyaar.service.categories.model.ProductSubCategoryDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class ProductCategoriesServiceImpl implements ProductCategoriesService {

    private final ProductCategoryRepository productCategoryRepository;
    private final ProductSubCategoryRepository productSubCategoryRepository;
    private final UserRepository userRespository;

    public ProductCategoriesServiceImpl(ProductCategoryRepository productCategoryRepository,
                                        ProductSubCategoryRepository productSubCategoryRepository,
                                        UserRepository userRespository) {
        this.productCategoryRepository = productCategoryRepository;
        this.productSubCategoryRepository = productSubCategoryRepository;
        this.userRespository = userRespository;
    }

    @Override
    public ProductCategoryDto storeProductCategory(ProductCategoryForm productCategoryForm) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = user.getUsername();
    	System.out.print(name);

        
        Optional<CustomerEntity> userEntity=userRespository.findByEmail(name);

        ProductCategoryDto productCategoryDto = new ProductCategoryDto.Builder()
                .productType(productCategoryForm.getProductType())
                .productTypeSlug(productCategoryForm.getProductTypeSlug())
                .description(productCategoryForm.getDescription())
                .isActive(true)
                .updatedBy(userEntity.get().getFirstName() +" " + userEntity.get().getLastName())
                .createdBy(userEntity.get().getFirstName() +" " + userEntity.get().getLastName())
                .build();
    	System.out.print(productCategoryDto.toString());

        productCategoryRepository.save(ProductCategoryEntity.of(productCategoryDto)).toProductCategoryDto();
        return   productCategoryRepository.save(ProductCategoryEntity.of(productCategoryDto)).toProductCategoryDto();

    }

    @Override
    public ProductSubCategoryDto storeProductSubCategory(ProductCategoryForm productCategoryForm) {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = user.getUsername();
        Optional<CustomerEntity> userEntity=userRespository.findByEmail(name);
       Optional< ProductCategoryEntity> productCategoryEntity = productCategoryRepository.findByProductCategoryId(productCategoryForm.getProductTypeId());
       if (productCategoryEntity.isPresent()){
        ProductSubCategoryDto productSubCategoryDto = new ProductSubCategoryDto.Builder()
                .productSubType(productCategoryForm.getProductType())
                .productSubTypeSlug(productCategoryForm.getProductTypeSlug())
                .productCategoryEntity(productCategoryEntity.get())
                .description(productCategoryForm.getDescription())
                .isActive(true)
                .updatedBy(userEntity.get().getFirstName() +" " + userEntity.get().getLastName())
                .createdBy(name)
                .build();
        return   productSubCategoryRepository.save(ProductSubCategoryEntity.of(productSubCategoryDto)).toProductSubCategoryDto();}
       return null;
    }

    @Override
    public Page<ProductCategoryDto> getAllProductCategory(Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<ProductCategoryDto> productCategoryDto = productCategoryRepository.findAllByOrderByCreatedDateAsc().stream()
                .map(productCategoryEntity -> productCategoryEntity.toProductCategoryDto())
                .collect(Collectors.toList());

        List<ProductCategoryDto> list;

        if (productCategoryDto.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, productCategoryDto.size());
            list = productCategoryDto.subList(startItem, toIndex);
        }

        Page<ProductCategoryDto> bookPage
                = new PageImpl<ProductCategoryDto>(list, PageRequest.of(currentPage, pageSize), productCategoryDto.size());

        return bookPage;
    }

    @Override
    public Page<ProductSubCategoryDto> getAllSubProductCategory(Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<ProductSubCategoryDto> productSubCategoryDto = productSubCategoryRepository.findAllByOrderByCreatedDateAsc().stream().map(productSubCategoryEntity ->
                productSubCategoryEntity.toProductSubCategoryDto()).collect(Collectors.toList());
        List<ProductSubCategoryDto> list;

        if (productSubCategoryDto.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, productSubCategoryDto.size());
            list = productSubCategoryDto.subList(startItem, toIndex);
        }

        Page<ProductSubCategoryDto> bookPage
                = new PageImpl<ProductSubCategoryDto>(list, PageRequest.of(currentPage, pageSize), productSubCategoryDto.size());

        return bookPage;
    }

    @Override
    public ProductCategoryDto getProductCategory(UUID productCategoryId) {
        Optional<ProductCategoryEntity> productCategoryEntity = productCategoryRepository.findByProductCategoryId(productCategoryId);
        if (productCategoryEntity.isPresent()) {
            return productCategoryEntity.get().toProductCategoryDto();
        }
        return null;
    }

    @Override
    public ProductSubCategoryDto getProductSubCategory(UUID productSubCategoryId) {
        Optional<ProductSubCategoryEntity> productSubCategoryEntity = productSubCategoryRepository.findByProductSubCategoryId(productSubCategoryId);
        if (productSubCategoryEntity.isPresent()) {
            return productSubCategoryEntity.get().toProductSubCategoryDto();
        }
        return null;
    }

    @Override
    public Page<ProductSubCategoryDto> getSpecificAllProductSubCategory(UUID ref, Pageable pageable) {

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<ProductSubCategoryDto> productSubCategoryDto = productSubCategoryRepository.findByProductCategoryEntityProductCategoryId(
                ref).stream()
                .map(ProductSubCategoryEntity::toProductSubCategoryDto).collect(Collectors.toList());
        List<ProductSubCategoryDto> list;

        if (productSubCategoryDto.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, productSubCategoryDto.size());
            list = productSubCategoryDto.subList(startItem, toIndex);
        }

        Page<ProductSubCategoryDto> bookPage
                = new PageImpl<ProductSubCategoryDto>(list, PageRequest.of(currentPage, pageSize), productSubCategoryDto.size());

        return bookPage;
    }

    /**
     * Generate Order Number
     * YyyyMMddHHmmss 6 4-bit random user id
     * @return public static String getGenerateOrderNo(Long userId){
    String time = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    StringBuffer stringBuffer = new StringBuffer(userId.toString());
    // id length is greater than four taken after 4
    if(stringBuffer.length()>4){
    stringBuffer = new StringBuffer(stringBuffer.substring(stringBuffer.length()-4,stringBuffer.length()));
    }
    // length less than the full complement four 4
    while (stringBuffer.length()<4){
    stringBuffer.insert(0,"0");
    }
    return time+RandomUtil.getNumber(6)+stringBuffer.toString();
    }*/
}
