package com.exceltech.excelyaar.service.categories;

import com.exceltech.excelyaar.controller.dashboard.resource.ProductCategoryForm;
import com.exceltech.excelyaar.data.entity.ProductCategoryEntity;
import com.exceltech.excelyaar.service.categories.model.ProductCategoryDto;
import com.exceltech.excelyaar.service.categories.model.ProductSubCategoryDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

public interface ProductCategoriesService {

    ProductCategoryDto storeProductCategory( ProductCategoryForm productCategoryForm);

    ProductSubCategoryDto storeProductSubCategory(ProductCategoryForm productCategoryForm);

    Page<ProductCategoryDto> getAllProductCategory(Pageable pageable);

    Page<ProductSubCategoryDto> getAllSubProductCategory(Pageable pageable);

    ProductCategoryDto getProductCategory(UUID  productCategoryId);
    ProductSubCategoryDto getProductSubCategory(UUID productSubCategoryId);

    Page<ProductSubCategoryDto> getSpecificAllProductSubCategory(UUID ref, Pageable pageable);
}
