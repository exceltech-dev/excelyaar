package com.exceltech.excelyaar.service.security;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.exceltech.excelyaar.data.dto.UserRegistrationDto;
import com.exceltech.excelyaar.data.entity.*;



public interface UserService extends UserDetailsService{
	CustomerEntity save(UserRegistrationDto registrationDto);
	CustomerEntity saveAdmin(UserRegistrationDto registrationDto);
}
