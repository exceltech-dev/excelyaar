package com.exceltech.excelyaar.service.security;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.exceltech.excelyaar.common.UserRole;
import com.exceltech.excelyaar.data.dto.UserRegistrationDto;
import com.exceltech.excelyaar.data.entity.CustomerEntity;
import com.exceltech.excelyaar.data.repository.*;



@Service
public class UserServiceImpl implements UserService{

	private UserRepository userRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	public UserServiceImpl(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	public CustomerEntity save(UserRegistrationDto registrationDto) {
		CustomerEntity user = new CustomerEntity(registrationDto.getFirstName(), 
				registrationDto.getLastName(), registrationDto.getEmail(),
				passwordEncoder.encode(registrationDto.getPassword()), UserRole.USER);
		
		return userRepository.save(user);
	}
	@Override
	public CustomerEntity saveAdmin(UserRegistrationDto registrationDto) {
		CustomerEntity user = new CustomerEntity(registrationDto.getFirstName(), 
				registrationDto.getLastName(), registrationDto.getEmail(),
				passwordEncoder.encode(registrationDto.getPassword()),  UserRole.ADMIN );
		
		return userRepository.save(user);
	}
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	
		Optional<CustomerEntity> user = userRepository.findByEmail(username);
		if(user.isEmpty()) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.get().getEmail(), user.get().getPassword(), mapRolesToAuthorities(user.get().getUserRole()));		
	}
	
	private Collection<? extends GrantedAuthority> mapRolesToAuthorities(UserRole role){
		final SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(role.name());
		return Collections.singletonList(simpleGrantedAuthority);
		//return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
	}
	
}
