package com.exceltech.excelyaar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude={SecurityAutoConfiguration.class})
@EntityScan("com.exceltech.excelyaar.data.entity")
public class ExcelyaarApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExcelyaarApplication.class, args);
	}

}
