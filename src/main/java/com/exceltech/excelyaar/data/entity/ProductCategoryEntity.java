package com.exceltech.excelyaar.data.entity;

import com.exceltech.excelyaar.service.categories.model.ProductCategoryDto;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(
        name = "product_category",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"product_category_id", "product_type"})
        }
)
public class ProductCategoryEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(
            name = "product_category_id",
            columnDefinition = "BINARY(16)",
            length = 256,
            nullable = false,
            unique = true)
    private UUID productCategoryId;

    @Column(name = "product_type",nullable = false)
    private String productType;

    @Column(name = "product_type_slug",nullable = false)
    private String productTypeSlug;

    @Column(name = "description",nullable = false)
    private String description;

    @Column(name = "created_by",nullable = false)
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "is_active",nullable = false)
    private Boolean isActive;

    @Column(name = "last_updated_date")
    private Date lastUpdatedDate;
    @Column(name = "created_date")
    private Date createdDate;


    public ProductCategoryEntity() {
        super();
    }

    public UUID getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(UUID productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }


    public String getProductTypeSlug() {
        return productTypeSlug;
    }

    public void setProductTypeSlug(String productTypeSlug) {
        this.productTypeSlug = productTypeSlug;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "ProductCategoryEntity [productCategoryId=" + productCategoryId + ", productType=" + productType
                + ", productTypeSlug=" + productTypeSlug + ", description=" + description + ", createdBy=" + createdBy
                + ", updatedBy=" + updatedBy + ", isActive=" + isActive + ", lastUpdatedDate=" + lastUpdatedDate
                + ", createdDate=" + createdDate + "]";
    }

    public static ProductCategoryEntity of(ProductCategoryDto productCategoryDto) {
        ProductCategoryEntity productCategoryEntity = new ProductCategoryEntity();
        productCategoryEntity.setProductType(productCategoryDto.getProductType());
        productCategoryEntity.setDescription(productCategoryDto.getDescription());
        productCategoryEntity.setProductTypeSlug(productCategoryDto.getProductTypeSlug());
        productCategoryEntity.setCreatedBy(productCategoryDto.getCreatedBy());
        productCategoryEntity.setUpdatedBy(productCategoryDto.getUpdatedBy());
        productCategoryEntity.setIsActive(productCategoryDto.getActive());
        final Date now = new Date();

        productCategoryEntity.setCreatedDate(now);
        productCategoryEntity.setLastUpdatedDate(now);
    return productCategoryEntity;
    }
    @Transient
    public ProductCategoryDto toProductCategoryDto() {
        return new ProductCategoryDto.Builder()
                .productType(this.productType)
                .productCategoryId(this.productCategoryId)
                .productTypeSlug(this.productTypeSlug)
                .createdBy(this.createdBy)
                .createdDate(this.createdDate)
                .description(this.description)
                .updatedBy(this.updatedBy)
                .isActive(this.isActive)
                .lastUpdatedDate(this.lastUpdatedDate)
                .build();
    }

}
