package com.exceltech.excelyaar.data.entity;

import com.exceltech.excelyaar.service.categories.model.ProductSubCategoryDto;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(
        name = "product_sub_category",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"product_sub_category_id", "Product_Sub_Type"})
        }
)

public class ProductSubCategoryEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(
            name = "product_sub_category_id",
            columnDefinition = "BINARY(16)",
            length = 256,
            nullable = false,
            unique = true)
    private UUID productSubCategoryId;

    @Column(name = "Product_Sub_Type")
    private String productSubType;

    @ManyToOne
    @JoinColumn(name = "product_category_id")
    private ProductCategoryEntity productCategoryEntity;

    @Column(name = "product_sub_type_slug")
    private String productSubTypeSlug;
    @Column(name = "description",nullable = false)
    private String description;

    @Column(name = "created_by",nullable = false)
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "is_active",nullable = false)
    private Boolean isActive;

    @Column(name = "last_updated_date")
    private Date lastUpdatedDate;
    @Column(name = "created_date")
    private Date createdDate;

    public UUID getProductSubCategoryId() {
        return productSubCategoryId;
    }

    public void setProductSubCategoryId(UUID productSubCategoryId) {
        this.productSubCategoryId = productSubCategoryId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getProductSubType() {
        return productSubType;
    }

    public void setProductSubType(String productSubType) {
        this.productSubType = productSubType;
    }

    public ProductCategoryEntity getProductCategoryEntity() {
        return productCategoryEntity;
    }

    public void setProductCategoryEntity(ProductCategoryEntity productCategoryEntity) {
        this.productCategoryEntity = productCategoryEntity;
    }

    public String getProductSubTypeSlug() {
        return productSubTypeSlug;
    }

    public void setProductSubTypeSlug(String productSubTypeSlug) {
        this.productSubTypeSlug = productSubTypeSlug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }
    public static ProductSubCategoryEntity of(ProductSubCategoryDto productSubCategoryDto) {
        ProductSubCategoryEntity productSubCategoryEntity = new ProductSubCategoryEntity();
        productSubCategoryEntity.setProductSubType(productSubCategoryDto.getProductSubType());
        productSubCategoryEntity.setProductCategoryEntity(productSubCategoryDto.getProductCategoryId());
        productSubCategoryEntity.setDescription(productSubCategoryDto.getDescription());
        productSubCategoryEntity.setProductSubTypeSlug(productSubCategoryDto.getProductSubTypeSlug());
        productSubCategoryEntity.setCreatedBy(productSubCategoryDto.getCreatedBy());
        productSubCategoryEntity.setUpdatedBy(productSubCategoryDto.getUpdatedBy());
        productSubCategoryEntity.setActive(productSubCategoryDto.getActive());
        final Date now = new Date();

        productSubCategoryEntity.setCreatedDate(now);
        productSubCategoryEntity.setLastUpdatedDate(now);
        return productSubCategoryEntity;
    }
    @Transient
    public ProductSubCategoryDto toProductSubCategoryDto() {
        return new ProductSubCategoryDto.Builder()
                .productSubType(this.productSubType)
                .productSubCategoryId(this.productSubCategoryId)
                .productCategoryEntity(this.productCategoryEntity)
                .productSubTypeSlug(this.productSubTypeSlug)
                .createdBy(this.createdBy)
                .createdDate(this.createdDate)
                .description(this.description)
                .updatedBy(this.updatedBy)
                .isActive(this.isActive)
                .lastUpdatedDate(this.lastUpdatedDate)
                .build();
    }

    @Override
    public String toString() {
        return "ProductSubCategoryEntity{" +
                "productSubCategoryId=" + productSubCategoryId +
                ", productSubType='" + productSubType + '\'' +
                ", productCategoryEntity=" + productCategoryEntity +
                ", productTypeSlug='" + productSubTypeSlug + '\'' +
                ", description='" + description + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", isActive=" + isActive +
                ", lastUpdatedDate=" + lastUpdatedDate +
                ", createdDate=" + createdDate +
                '}';
    }
}
