package com.exceltech.excelyaar.data.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name =  "vendor", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
public class VendorEntity {

	
	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    private CustomerEntity customerEntity;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "store_name")
	private String storeName;
	@Column(name = "email")
	private String email;
	
	@Column(name = "description")
	private String description;

	@Column(nullable = true,name = "self_photo_image")
    private String selfPhotoImage;

	@Column(name = "id_Photo_image")
	private String idPhotoImage;

	@Column(name = "approved")
	private Boolean approved;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "updated_date")
	private Date updateDate;


	public VendorEntity() {
	}

	public VendorEntity(  String firstName, String lastName, String email,String storeName, String description, Boolean approved,
			String selfPhotoImage,
			String idPhotoImage,
			Date createdDate,
			Date updateDate) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.storeName = storeName;
		this.description = description;
		this.approved = approved;
		this.selfPhotoImage = selfPhotoImage;
		this.idPhotoImage = idPhotoImage;
		this.updateDate = updateDate;
		this.createdDate = createdDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	


	public CustomerEntity getCustomerEntity() {
		return customerEntity;
	}

	public void setCustomerEntity(CustomerEntity customerEntity) {
		this.customerEntity = customerEntity;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	
	public String getSelfPhotoImage() {
		return selfPhotoImage;
	}

	public void setSelfPhotoImage(String selfPhotoImage) {
		this.selfPhotoImage = selfPhotoImage;
	}

	public String getIdPhotoImage() {
		return idPhotoImage;
	}

	public void setIdPhotoImage(String idCardImage) {
		this.idPhotoImage = idCardImage;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "VendorEntity [id=" + id + ", customerEntity=" + customerEntity + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", storeName=" + storeName + ", email=" + email + ", description="
				+ description + ", selfPhotoImage=" + selfPhotoImage + ", idPhotoImage=" + idPhotoImage + ", approved="
				+ approved + ", createdDate=" + createdDate + ", updateDate=" + updateDate + "]";
	}
	 @Transient
	    public String getIdPhotosImagePath() {
	        if (idPhotoImage == null || id == null) return null;
	         
	        return "/Vendeurs/" + id + "/idCard/" + idPhotoImage;
	    }
	 @Transient
	    public String getSelfPhotosImagePath() {
	        if (idPhotoImage == null || id == null) return null;
	         
	        return "/Vendeurs/" + id + "/selfPhoto/" + selfPhotoImage;
	    }
}
