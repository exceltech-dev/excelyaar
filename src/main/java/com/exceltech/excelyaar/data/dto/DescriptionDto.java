package com.exceltech.excelyaar.data.dto;

public class DescriptionDto {
	private Long vendorId;

	public DescriptionDto() {
		super();
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}


}
