package com.exceltech.excelyaar.data.dto;

import java.util.Date;

import javax.persistence.Column;

import org.springframework.web.multipart.MultipartFile;

public class BecomeVendorDto {
	private String firstName;
	private String lastName;
	private String storeName;
	private String email;
	private String description;
    private MultipartFile selfPhotoImage;
	private MultipartFile idPhotoImage;
	private Boolean approved;
	private Date createdDate;
	private Date updateDate;
	
	public BecomeVendorDto() {
	}


	public BecomeVendorDto(
			String firstName, 
			String lastName,
			String storeName, 
			String email, 
			String description,
			MultipartFile selfPhotoImage,
			MultipartFile idCardImage,
			Boolean approved,
			Date createdDate,
			Date updateDate) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.storeName = storeName;
		this.email = email;
		this.description = description;
		this.selfPhotoImage = selfPhotoImage;
		this.idPhotoImage = idCardImage;
		this.approved =approved;
		this.updateDate = updateDate;
		this.createdDate = createdDate;
		
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}




	public void setLastName(String lastName) {
		this.lastName = lastName;
	}




	public String getStoreName() {
		return storeName;
	}


	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public MultipartFile getSelfPhotoImage() {
		return selfPhotoImage;
	}


	public void setSelfPhotoImage(MultipartFile selfPhotoImage) {
		this.selfPhotoImage = selfPhotoImage;
	}


	public MultipartFile getIdPhotoImage() {
		return idPhotoImage;
	}


	public void setIdPhotoImage(MultipartFile idCardImage) {
		this.idPhotoImage = idCardImage;
	}


	public Boolean getApproved() {
		return approved;
	}


	public void setApproved(Boolean approved) {
		this.approved = approved;
	}


	public Date getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
	
}
