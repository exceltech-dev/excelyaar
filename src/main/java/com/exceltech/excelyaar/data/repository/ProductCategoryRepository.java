package com.exceltech.excelyaar.data.repository;

import com.exceltech.excelyaar.data.entity.ProductCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductCategoryRepository  extends JpaRepository<ProductCategoryEntity, UUID> {

    List<ProductCategoryEntity> findAllByOrderByCreatedDateAsc();
    Optional<ProductCategoryEntity> findByProductCategoryId(UUID productCategoryId);
}
