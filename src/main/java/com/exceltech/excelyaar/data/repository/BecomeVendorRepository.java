package com.exceltech.excelyaar.data.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exceltech.excelyaar.data.entity.*;


@Repository
public interface BecomeVendorRepository extends JpaRepository<VendorEntity, Long> {
	List<VendorEntity>findAll();
	Optional<VendorEntity> findById(Long id);
}
