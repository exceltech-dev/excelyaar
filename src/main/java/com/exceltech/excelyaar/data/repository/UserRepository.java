package com.exceltech.excelyaar.data.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exceltech.excelyaar.data.entity.*;


@Repository
public interface UserRepository extends JpaRepository<CustomerEntity, Long>{
	Optional<CustomerEntity> findByEmail(String email);
}