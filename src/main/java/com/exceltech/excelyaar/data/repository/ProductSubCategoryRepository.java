package com.exceltech.excelyaar.data.repository;

import com.exceltech.excelyaar.data.entity.ProductCategoryEntity;
import com.exceltech.excelyaar.data.entity.ProductSubCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductSubCategoryRepository extends JpaRepository<ProductSubCategoryEntity, UUID> {

    List<ProductSubCategoryEntity> findAllByOrderByCreatedDateAsc();
    Optional<ProductSubCategoryEntity> findByProductSubCategoryId(UUID productSubCategoryId);
    List<ProductSubCategoryEntity> findByProductCategoryEntityProductCategoryId(UUID productCategoryId);
}
