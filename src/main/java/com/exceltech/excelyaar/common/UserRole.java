package com.exceltech.excelyaar.common;

public enum UserRole {

	ADMIN, USER, VENDOR;
}
